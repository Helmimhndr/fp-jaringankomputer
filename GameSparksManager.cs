﻿using System;

private void OnPacketReceived(RTPacket _packet)
{

    switch (_packet.OpCode)
    {
        
        case 1:
            if (chatManager == null)
            { 
                chatManager = GameObject.Find("Chat Manager").GetComponent<ChatManager>();
            }
            chatManager.OnMessageReceived(_packet); 
            break;

        case 2:

            GameController.Instance().UpdateOpponentTanks(_packet);
            break;

        case 3:
            GameController.Instance().InstantiateOpponentShells(_packet);
            break;

        case 4:
            GameController.Instance().UpdateOpponentShells(_packet);
            break;

        case 5:
            GameController.Instance().RegisterOpponentCollision(_packet);
            break;

    }
}

