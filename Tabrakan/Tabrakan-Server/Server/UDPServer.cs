﻿using GPNetworkMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GPNetworkServer
{
  public class UDPServer : IServer
  {
    private string ipAddressValue = "";
    private Queue<string> messageQueue = new Queue<string>();
    private List<UDPServer.Client> userList = new List<UDPServer.Client>();
    private int portNumber;
    private UdpClient listener;
    private Thread listenThread;
    private bool isListening;

    public UDPServer()
    {
    }

    public UDPServer(string hostname, int port)
    {
      this.ipAddressValue = hostname;
      this.portNumber = port;
      this.listener = new UdpClient(new IPEndPoint(IPAddress.Parse(this.ipAddressValue), this.portNumber));
      this.listenThread = new Thread(new ThreadStart(this.ListenMethod));
    }

    public UDPServer(bool UseLoopback, int port)
    {
      this.ipAddressValue = (UseLoopback ? (object) IPAddress.Loopback : (object) this.LocalIPAddress()).ToString();
      this.portNumber = port;
      this.listener = new UdpClient(new IPEndPoint(IPAddress.Parse(this.ipAddressValue), this.portNumber));
      this.listenThread = new Thread(new ThreadStart(this.ListenMethod));
    }

    public void SendMessageToAll(AMessage sendMessage)
    {
      if (sendMessage.Type == MessageType.INFOEXCEPT1 || sendMessage.Type == MessageType.INFOTO1)
        sendMessage.Type = MessageType.INFO;
      byte[] bytes = Encoding.ASCII.GetBytes(sendMessage.ID.ToString() + ";" + sendMessage.Type.ToString() + ";" + sendMessage.Message);
      foreach (UDPServer.Client user in this.userList)
        this.listener.Send(bytes, bytes.Length, user.endPoint);
    }

    public void SendMessageToAllButOne(AMessage sendMessage, int ClientNumber)
    {
      sendMessage.Type = MessageType.INFO;
      byte[] bytes = Encoding.ASCII.GetBytes(sendMessage.ID.ToString() + ";" + sendMessage.Type.ToString() + ";" + sendMessage.Message);
      foreach (UDPServer.Client user in this.userList)
      {
        if (ClientNumber != user.ClientNumber)
          this.listener.Send(bytes, bytes.Length, user.endPoint);
      }
    }

    public void SendMessageToOne(AMessage sendMessage, int ClientNumber)
    {
      sendMessage.Type = MessageType.INFO;
      byte[] bytes = Encoding.ASCII.GetBytes(sendMessage.ID.ToString() + ";" + sendMessage.Type.ToString() + ";" + sendMessage.Message);
      foreach (UDPServer.Client user in this.userList)
      {
        if (user.ClientNumber == ClientNumber)
        {
          this.listener.Send(bytes, bytes.Length, user.endPoint);
          break;
        }
      }
    }

    public void Start()
    {
      this.isListening = true;
      if (this.listenThread != null && this.listenThread.IsAlive)
      {
        if (this.listenThread == null)
          return;
        this.listenThread.Abort();
        this.listenThread.Join();
      }
      else
        this.listenThread.Start();
    }

    public void Stop()
    {
      this.SendMessageToAll(new AMessage(MessageType.LEAVE, -1, "Server disconnected"));
      this.isListening = false;
      this.userList.Clear();
      if (this.listener == null)
        return;
      this.listener.Client.Shutdown(SocketShutdown.Both);
      this.listener.Client.Close();
      this.listener = (UdpClient) null;
    }

    private void ListenMethod()
    {
      try
      {
        while (this.isListening)
        {
          AMessage sendMessage = new AMessage(MessageType.ANY, 0, "");
          IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
          byte[] bytes1 = this.listener.Receive(ref remoteEP);
          if (bytes1 == null || bytes1.Length == 0)
            break;
          string str1 = Encoding.ASCII.GetString(bytes1);
          string[] strArray = str1.Split(';');
          sendMessage.ID = Convert.ToInt32(strArray[0]);
          sendMessage.Type = (MessageType) System.Enum.Parse(typeof (MessageType), strArray[1]);
          sendMessage.Message = str1.Substring((strArray[0] + ";" + strArray[1] + ";").Length);
          if (sendMessage.Type == MessageType.JOIN)
          {
            UDPServer.Client client = new UDPServer.Client(remoteEP, this.AssignClientID(this.userList.Count + 1));
            sendMessage.ID = client.ClientNumber;
            byte[] bytes2 = Encoding.ASCII.GetBytes(sendMessage.ID.ToString() + ";" + sendMessage.Type.ToString() + ";" + sendMessage.Message);
            this.listener.Send(bytes2, bytes2.Length, remoteEP);
            this.SendMessageToAll(sendMessage);
            this.userList.Add(client);
            this.messageQueue.Enqueue("Client " + (object) client.ClientNumber + " Connected");
          }
          else if (sendMessage.Type == MessageType.LEAVE)
          {
            foreach (UDPServer.Client user in this.userList)
            {
              if (user.ClientNumber == sendMessage.ID)
              {
                this.userList.Remove(user);
                break;
              }
            }
            this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " Removed");
            this.SendMessageToAll(sendMessage);
          }
          else if (sendMessage.Type == MessageType.CLIENTCOUNT)
          {
            this.messageQueue.Enqueue("Client Count Request From: " + (object) sendMessage.ID);
            sendMessage.Message = string.Concat((object) this.userList.Count);
            this.SendMessageToAll(sendMessage);
          }
          else if (sendMessage.Type == MessageType.INFOTO1)
          {
            string str2 = sendMessage.Message.Split(';')[0];
            sendMessage.Message = sendMessage.Message.Substring(str2.Length + 1);
            this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - To: " + str2);
            this.SendMessageToOne(sendMessage, Convert.ToInt32(str2));
          }
          else if (sendMessage.Type == MessageType.INFOEXCEPT1)
          {
            string str2 = sendMessage.Message.Split(';')[0];
            sendMessage.Message = sendMessage.Message.Substring(str2.Length + 1);
            this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - Except: " + str2);
            this.SendMessageToAllButOne(sendMessage, Convert.ToInt32(str2));
          }
          else
          {
            this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - To All");
            this.SendMessageToAll(sendMessage);
          }
        }
      }
      catch (SocketException ex)
      {
        if (!this.isListening)
          return;
        this.messageQueue.Enqueue(ex.Message + ": " + (object) ex.TargetSite);
      }
    }

    private int AssignClientID(int initID)
    {
      if (initID > 1)
      {
        List<int> intList = new List<int>();
        foreach (UDPServer.Client user in this.userList)
          intList.Add(user.ClientNumber);
        intList.Sort();
        if (intList.Contains(initID))
        {
          for (int index = initID; index >= 1; --index)
          {
            if (!intList.Contains(index))
            {
              initID = index;
              break;
            }
          }
        }
      }
      return initID;
    }

    private IPAddress LocalIPAddress()
    {
      if (!NetworkInterface.GetIsNetworkAvailable())
        return (IPAddress) null;
      return ((IEnumerable<IPAddress>) Dns.GetHostEntry(Dns.GetHostName()).AddressList).LastOrDefault<IPAddress>((Func<IPAddress, bool>) (ip => ip.AddressFamily == AddressFamily.InterNetwork));
    }

    public string IPAddressValue
    {
      get
      {
        return this.ipAddressValue;
      }
    }

    public int PortNumber
    {
      get
      {
        return this.portNumber;
      }
    }

    public Queue<string> MessageQueue
    {
      get
      {
        return this.messageQueue;
      }
    }

    private class Client
    {
      public IPEndPoint endPoint;
      public int ClientNumber;

      public Client(IPEndPoint endPoint, int clientNumber)
      {
        this.endPoint = endPoint;
        this.ClientNumber = clientNumber;
      }
    }
  }
}
