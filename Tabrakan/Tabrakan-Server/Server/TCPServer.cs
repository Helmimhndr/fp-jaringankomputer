﻿using GPNetworkMessage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace GPNetworkServer
{
  public class TCPServer : IServer
  {
    private string ipAddressValue = "";
    private Queue<string> messageQueue = new Queue<string>();
    private BinaryFormatter binary = new BinaryFormatter();
    private List<TCPServer.Client> userList = new List<TCPServer.Client>();
    private int portNumber;
    private TcpListener tcpListener;
    private Thread listenThread;
    private bool isListening;

    public TCPServer()
    {
    }

    public TCPServer(string hostname, int port)
    {
      this.ipAddressValue = hostname;
      this.portNumber = port;
      this.tcpListener = new TcpListener(IPAddress.Parse(hostname), port);
      this.listenThread = new Thread(new ThreadStart(this.ListenMethod));
    }

    public TCPServer(bool UseLoopback, int port)
    {
      IPAddress localaddr = UseLoopback ? IPAddress.Loopback : this.LocalIPAddress();
      this.ipAddressValue = localaddr.ToString();
      this.portNumber = port;
      this.tcpListener = new TcpListener(localaddr, port);
      this.listenThread = new Thread(new ThreadStart(this.ListenMethod));
    }

    public void Start()
    {
      if (this.listenThread != null && this.listenThread.IsAlive)
      {
        this.listenThread.Abort();
        this.listenThread.Join();
      }
      else
        this.listenThread.Start();
    }

    public void Stop()
    {
      this.SendMessageToAll(new AMessage(MessageType.LEAVE, -1, "Server disconnected"));
      this.userList.Clear();
      this.isListening = false;
      if (this.tcpListener == null)
        return;
      this.tcpListener.Stop();
    }

    private void ListenMethod()
    {
      this.isListening = true;
      this.userList.Clear();
      try
      {
        this.tcpListener.Start();
      }
      catch (Exception ex)
      {
        this.messageQueue.Enqueue(ex.Message + ": " + (object) ex.TargetSite);
        this.tcpListener.Stop();
      }
      try
      {
        while (this.isListening)
        {
          if (!this.tcpListener.Pending())
          {
            Thread.Sleep(500);
          }
          else
          {
            TcpClient client1 = this.tcpListener.AcceptTcpClient();
            NetworkStream stream = client1.GetStream();
            int num = this.AssignClientID(this.userList.Count + 1);
            TCPServer.Client client2 = new TCPServer.Client(client1, stream, num);
            this.binary.Serialize((Stream) stream, (object) new AMessage(MessageType.JOIN, num, "Client Connected Successfully"));
            this.messageQueue.Enqueue("Client " + (object) client2.ClientNumber + " Connected");
            AMessage amessage = this.binary.Deserialize((Stream) stream) as AMessage;
            foreach (TCPServer.Client user in this.userList)
            {
              this.binary.Serialize((Stream) user.Stream, (object) amessage);
              user.Stream.Flush();
            }
            this.userList.Add(client2);
            client2.Thread = new Thread(new ParameterizedThreadStart(this.ClientMethod));
            client2.Thread.Start((object) client2);
          }
        }
      }
      catch (Exception ex)
      {
        this.messageQueue.Enqueue(ex.Message + ": " + (object) ex.TargetSite);
        this.Stop();
      }
    }

    private int AssignClientID(int initID)
    {
      if (initID > 1)
      {
        List<int> intList = new List<int>();
        foreach (TCPServer.Client user in this.userList)
          intList.Add(user.ClientNumber);
        intList.Sort();
        if (intList.Contains(initID))
        {
          for (int index = initID; index >= 1; --index)
          {
            if (!intList.Contains(index))
            {
              initID = index;
              break;
            }
          }
        }
      }
      return initID;
    }

    private void ClientMethod(object obj)
    {
      TCPServer.Client client = obj as TCPServer.Client;
      if (client == null)
        return;
      try
      {
        while (client.Connected)
        {
          AMessage sendMessage = this.binary.Deserialize((Stream) client.Stream) as AMessage;
          if (sendMessage != null)
          {
            if (sendMessage.Type == MessageType.LEAVE && sendMessage.ID == client.ClientNumber)
            {
              client.Connected = false;
              this.userList.Remove(client);
            }
            if (sendMessage.Type == MessageType.CLIENTCOUNT)
            {
              this.messageQueue.Enqueue("Client Count Request From: " + (object) sendMessage.ID);
              sendMessage.Message = string.Concat((object) this.userList.Count);
              this.SendMessageToAll(sendMessage);
            }
            else if (sendMessage.Type == MessageType.INFOTO1)
            {
              string str = sendMessage.Message.Split(';')[0];
              sendMessage.Message = sendMessage.Message.Substring(str.Length + 1);
              this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - To: " + str);
              this.SendMessageToOne(sendMessage, Convert.ToInt32(str));
            }
            else if (sendMessage.Type == MessageType.INFOEXCEPT1)
            {
              string str = sendMessage.Message.Split(';')[0];
              sendMessage.Message = sendMessage.Message.Substring(str.Length + 1);
              this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - Except: " + str);
              this.SendMessageToAllButOne(sendMessage, Convert.ToInt32(str));
            }
            else
            {
              this.messageQueue.Enqueue("Client " + (object) sendMessage.ID + " - " + sendMessage.Message + " - To All");
              this.SendMessageToAll(sendMessage);
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.messageQueue.Enqueue(ex.Message + ": " + (object) ex.TargetSite);
      }
      finally
      {
        this.userList.Remove(client);
        this.messageQueue.Enqueue("Client " + (object) client.ClientNumber + " Removed");
      }
    }

    public void SendMessageToAll(AMessage sendMessage)
    {
      if (sendMessage.Type == MessageType.INFOEXCEPT1 || sendMessage.Type == MessageType.INFOTO1)
        sendMessage.Type = MessageType.INFO;
      foreach (TCPServer.Client user in this.userList)
      {
        this.binary.Serialize((Stream) user.Stream, (object) sendMessage);
        user.Stream.Flush();
      }
    }

    public void SendMessageToOne(AMessage sendMessage, int ClientNumber)
    {
      sendMessage.Type = MessageType.INFO;
      foreach (TCPServer.Client user in this.userList)
      {
        if (user.ClientNumber == ClientNumber)
        {
          this.binary.Serialize((Stream) user.Stream, (object) sendMessage);
          user.Stream.Flush();
          break;
        }
      }
    }

    public void SendMessageToAllButOne(AMessage sendMessage, int ClientNumber)
    {
      sendMessage.Type = MessageType.INFO;
      foreach (TCPServer.Client user in this.userList)
      {
        if (user.ClientNumber != ClientNumber)
        {
          this.binary.Serialize((Stream) user.Stream, (object) sendMessage);
          user.Stream.Flush();
          break;
        }
      }
    }

    private IPAddress LocalIPAddress()
    {
      if (!NetworkInterface.GetIsNetworkAvailable())
        return (IPAddress) null;
      return ((IEnumerable<IPAddress>) Dns.GetHostEntry(Dns.GetHostName()).AddressList).LastOrDefault<IPAddress>((Func<IPAddress, bool>) (ip => ip.AddressFamily == AddressFamily.InterNetwork));
    }

    public string IPAddressValue
    {
      get
      {
        return this.ipAddressValue;
      }
    }

    public int PortNumber
    {
      get
      {
        return this.portNumber;
      }
    }

    public Queue<string> MessageQueue
    {
      get
      {
        return this.messageQueue;
      }
    }

    private class Client
    {
      public TcpClient tcpClient;
      public NetworkStream Stream;
      public Thread Thread;
      public int ClientNumber;
      public bool Connected;

      public Client(TcpClient client, NetworkStream stream, int clientNumber)
      {
        this.tcpClient = client;
        this.Stream = stream;
        this.ClientNumber = clientNumber;
        this.Connected = true;
      }

      ~Client()
      {
        this.Connected = false;
        this.tcpClient.Close();
        this.Stream.Close();
        this.Thread.Abort();
        this.Thread.Join();
      }
    }
  }
}
