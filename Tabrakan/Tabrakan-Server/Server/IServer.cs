﻿using GPNetworkMessage;
using System.Collections.Generic;

namespace GPNetworkServer
{
  public interface IServer
  {
    string IPAddressValue { get; }

    int PortNumber { get; }

    Queue<string> MessageQueue { get; }

    void SendMessageToAll(AMessage sendMessage);

    void SendMessageToAllButOne(AMessage sendMessage, int ClientNumber);

    void SendMessageToOne(AMessage sendMessage, int ClientNumber);

    void Start();

    void Stop();
  }
}
