﻿namespace GPNetworkMessage
{
  public enum MessageType
  {
    ANY,
    JOIN,
    LEAVE,
    INFO,
    INFOTO1,
    INFOEXCEPT1,
    ERROR,
    CLIENTCOUNT,
  }
}
