﻿using System;

namespace GPNetworkMessage
{
  [Serializable]
  public class AMessage
  {
    public string Message = "";
    public MessageType Type;
    public int ID;

    public AMessage(MessageType Type, int ID, string Message)
    {
      this.Type = Type;
      this.ID = ID;
      this.Message = Message;
    }
  }
}
